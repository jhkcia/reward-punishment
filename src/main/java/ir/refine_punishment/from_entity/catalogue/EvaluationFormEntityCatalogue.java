package ir.refine_punishment.from_entity.catalogue;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ir.refine_punishment.from_entity.dao.EvaluationFormEntityDAO;
import ir.refine_punishment.from_entity.model.EvaluationFormEntity;


public class EvaluationFormEntityCatalogue {

	@Autowired
	EvaluationFormEntityDAO dao;

	public void add(EvaluationFormEntity entity) {
		dao.add(entity);
	}

	public EvaluationFormEntity getById(Long id) {
		return dao.getById(id);
	}

	public List<EvaluationFormEntity> getAll() {
		return dao.getAll();
	}

	public void update(EvaluationFormEntity entity) {
		dao.update(entity);
	}

	public void delete(Long id) {
		dao.delete(id);
	}

	public EvaluationFormEntity getByOwnerId(Long ownerId) {
		return dao.getByOwnerId(ownerId);
	}
}
