package ir.refine_punishment.from_entity.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import ir.refine_punishment.core.persistence.BaseHibernateEntityDAO;
import ir.refine_punishment.from_entity.model.EvaluationFormEntity;

public class EvaluationFormEntityDAO extends BaseHibernateEntityDAO<EvaluationFormEntity> {

	public EvaluationFormEntityDAO() {
		super(EvaluationFormEntity.class);
	}

	public EvaluationFormEntity getByOwnerId(Long ownerId) {
		Session session = this.sessionFactory.openSession();
		Query query = session.createQuery("From EvaluationFromEntity where ownerId = :ownerId");
		query.setParameter("ownerId", ownerId);
		List<EvaluationFormEntity> result = (List<EvaluationFormEntity>) query.list();
		session.close();
		return getFirstOrNull(result);
	}

}
