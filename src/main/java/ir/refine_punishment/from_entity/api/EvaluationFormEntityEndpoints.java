package ir.refine_punishment.from_entity.api;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;

import ir.refine_punishment.core.api.BaseEndPoint;
import ir.refine_punishment.core.auth.SecuredEndPoint;
import ir.refine_punishment.from_entity.catalogue.EvaluationFormEntityCatalogue;
import ir.refine_punishment.from_entity.model.EvaluationFormEntity;
import ir.refine_punishment.user.model.Role;

@Path("/evaluation_form_entity")
public class EvaluationFormEntityEndpoints extends BaseEndPoint {
	@Autowired
	protected EvaluationFormEntityCatalogue evaluationFormEntityCatalogue;

	@POST
	@Path("add")
	@SecuredEndPoint({ Role.EVALUATOR })
	public EvaluationFormEntity add(EvaluationFormEntity evaluationFormEntity) {
		evaluationFormEntity.fillElementsResults();
		evaluationFormEntityCatalogue.add(evaluationFormEntity);
		return evaluationFormEntityCatalogue.getById(evaluationFormEntity.getId());
	}

	@GET
	@Path("get")
	@SecuredEndPoint({ Role.ADMIN })
	public EvaluationFormEntity get(@QueryParam("id") Long id) {
		return evaluationFormEntityCatalogue.getById(id);
	}

	@GET
	@Path("list")
	@SecuredEndPoint({ Role.ADMIN })
	public List<EvaluationFormEntity> list() throws JsonProcessingException {
		List<EvaluationFormEntity> list = evaluationFormEntityCatalogue.getAll();
		return list;
	}
	@DELETE
	@Path("delete")
	@SecuredEndPoint({ Role.ADMIN })
	public void delete(@QueryParam("id") Long evaluationFormEntityId) {
		evaluationFormEntityCatalogue.delete(evaluationFormEntityId);
	}

	@POST
	@Path("update")
	@SecuredEndPoint({ Role.ADMIN })
	public EvaluationFormEntity update(EvaluationFormEntity evaluationFormEntity) {
		evaluationFormEntityCatalogue.update(evaluationFormEntity);
		evaluationFormEntity.fillElementsResults();
		return evaluationFormEntityCatalogue.getById(evaluationFormEntity.getId());
	}

}
