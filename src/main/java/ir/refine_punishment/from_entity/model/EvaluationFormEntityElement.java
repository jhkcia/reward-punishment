package ir.refine_punishment.from_entity.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
@Entity
@Table(name = "evaluation_form_entity_element")
public class EvaluationFormEntityElement {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "element_id")
	private Long id;
	
	@Column(name = "result")
	private String result;
	
	@Column(name = "value")
	private String value;

	@Column(name = "application_method_id")
	private Long applicationMethodId;

	@Column(name = "entity_element_id", nullable = false)
	private Long entityElementId;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getResult() {
		return result;
	}

	public void setResult(String result) {
		this.result = result;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Long getApplicationMethodId() {
		return applicationMethodId;
	}

	public void setApplicationMethodId(Long applicationMethodId) {
		this.applicationMethodId = applicationMethodId;
	}

	public Long getEntityElementId() {
		return entityElementId;
	}

	public void setEntityElementId(Long entityElementId) {
		this.entityElementId = entityElementId;
	}

	
}
