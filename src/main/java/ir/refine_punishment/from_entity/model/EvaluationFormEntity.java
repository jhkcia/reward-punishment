package ir.refine_punishment.from_entity.model;

import java.util.HashMap;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import ir.refine_punishment.from_type.model.AssessmentAndInterpretationMethod;
import ir.refine_punishment.from_type.model.EvaluationFormType;
import ir.refine_punishment.from_type.model.EvaluationFormTypeElement;

@Entity
@Table(name = "evaluation_form_entity")
public class EvaluationFormEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "entity_id")
	private Long id;

	@Column(name = "evaluator_id")
	private Long evaluatorId;
	
	@Column(name = "owner_id")
	private Long ownerId;
	
	@Column(name = "form_type_id")
	private Long formTypeId;
	
	@OneToMany(fetch = FetchType.EAGER, orphanRemoval = true)
	@JoinColumn(name = "entity_element_id")
	@Cascade({ CascadeType.ALL })
	private Set<EvaluationFormEntityElement> elements;

	public void fillElementsResults() {
		EvaluationFormType formType = null;
		HashMap<Long, AssessmentAndInterpretationMethod> formApplicationMethods = new HashMap<>();
		for (EvaluationFormTypeElement element : formType.getElements()) {
			for (AssessmentAndInterpretationMethod method : element.getAssessmentAndInterpretationMethods()) {
				formApplicationMethods.put(method.getId(), method);
			}
		}
		for (EvaluationFormEntityElement elemet : elements) {
			Long applicationMethodId = elemet.getApplicationMethodId();
			String result = formApplicationMethods.get(applicationMethodId).interpret(elemet.getValue());
			elemet.setResult(result);
		}
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getEvaluatorId() {
		return evaluatorId;
	}

	public void setEvaluatorId(Long evaluatorId) {
		this.evaluatorId = evaluatorId;
	}

	public Long getOwnerId() {
		return ownerId;
	}

	public void setOwnerId(Long ownerId) {
		this.ownerId = ownerId;
	}

	public Long getFormTypeId() {
		return formTypeId;
	}

	public void setFormTypeId(Long formTypeId) {
		this.formTypeId = formTypeId;
	}

	public Set<EvaluationFormEntityElement> getElements() {
		return elements;
	}

	public void setElements(Set<EvaluationFormEntityElement> elements) {
		this.elements = elements;
	}

}
