package ir.refine_punishment.from_type.dao;

import org.hibernate.Session;
import org.hibernate.Transaction;

import ir.refine_punishment.core.persistence.BaseHibernateEntityDAO;
import ir.refine_punishment.from_type.model.EvaluationFormType;
import ir.refine_punishment.from_type.model.EvaluationFormTypeElement;

public class EvaluationFormTypeDAO extends BaseHibernateEntityDAO<EvaluationFormType> {

	public EvaluationFormTypeDAO() {
		super(EvaluationFormType.class);
	}

	public void addElementToFormType(EvaluationFormTypeElement element) {
		Session session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.save(element);
		tx.commit();
		session.close();
	}

	public void deleteElementFromFormType(String elementId) {
		
	}
}
