package ir.refine_punishment.from_type.model;

public enum EvaluationInterpretion {
	NoRewardAndPunishment, Punishment, Reward;
}
