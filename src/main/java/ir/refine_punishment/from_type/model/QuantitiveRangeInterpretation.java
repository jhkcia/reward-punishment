package ir.refine_punishment.from_type.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = QuantitiveRangeInterpretation.TABLE_NAME)

public class QuantitiveRangeInterpretation {
	public static final String TABLE_NAME = "quantitive_range_interpretion";
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	protected Long id;

	@Column(name = "application_method_id", nullable = false)
	private Long applicationMethodId;

	@Column(name = "lower_bound", nullable = false)
	private int lowerBound;

	@Column(name = "upper_bound", nullable = false)
	private int upperBound;

	@Column(name = "interpretation")
	@Enumerated(EnumType.STRING)
	private EvaluationInterpretion interpretation;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getLowerBound() {
		return lowerBound;
	}

	public void setLowerBound(int lowerBound) {
		this.lowerBound = lowerBound;
	}

	public int getUpperBound() {
		return upperBound;
	}

	public void setUpperBound(int upperBound) {
		this.upperBound = upperBound;
	}

	public EvaluationInterpretion getInterpretation() {
		return interpretation;
	}

	public void setInterpretation(EvaluationInterpretion interpretation) {
		this.interpretation = interpretation;
	}

	public Long getApplicationMethodId() {
		return applicationMethodId;
	}

	public void setApplicationMethodId(Long applicationMethodId) {
		this.applicationMethodId = applicationMethodId;
	}

	@Override
	public String toString() {
		return "QuantitiveRangeInterpretation [id=" + id + ", lowerBound=" + lowerBound + ", upperBound=" + upperBound
				+ ", interpretation=" + interpretation + "]";
	}

}
