package ir.refine_punishment.from_type.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "qualitative_value_interpretion")

public class QualitativeValueInterpretation {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	protected Long id;

	@Column(name = "application_method_id")
	private Long applicationMethodId;

	@Column(name = "value")
	private String value;
	
	@Column(name = "description")
	private String description;
	
	@Column(name = "interpretation")
	@Enumerated(EnumType.STRING)
	private EvaluationInterpretion interpretation;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Long getApplicationMethodId() {
		return applicationMethodId;
	}

	public void setApplicationMethodId(Long applicationMethodId) {
		this.applicationMethodId = applicationMethodId;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public EvaluationInterpretion getInterpretation() {
		return interpretation;
	}

	public void setInterpretation(EvaluationInterpretion interpretation) {
		this.interpretation = interpretation;
	}

	@Override
	public String toString() {
		return "QualitativeValueInterpretation [id=" + id + ", applicationMethod=" + ", value=" + value
				+ ", description=" + description + ", interpretation=" + interpretation + "]";
	}

}
