package ir.refine_punishment.from_type.model;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Inheritance;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;

@Entity
@Inheritance
@DiscriminatorColumn(name = "type")
@Table(name = "assessment_interpretation_method")
@JsonIgnoreProperties(ignoreUnknown = true)

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @Type(value = QuantitiveApplicationMethod.class, name = "Quantitive"),
		@Type(value = QualitativeApplicationMethod.class, name = "Qualitative") })
public abstract class AssessmentAndInterpretationMethod {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	protected Long id;

	@Column(name = "form_element_id", nullable = false)
	private Long formElementId;

	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}

	public Long getFormElementId() {
		return formElementId;
	}

	public void setFormElementId(Long formElementId) {
		this.formElementId = formElementId;
	}

	public abstract String interpret(String value);
}
