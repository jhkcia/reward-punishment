package ir.refine_punishment.from_type.model;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "evaluation_form_type")
public class EvaluationFormType {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "form_id")
	protected Long id;

	@Column(name = "name", nullable = false, length = 50)
	private String name;

	@OneToMany(fetch = FetchType.EAGER ,  cascade = CascadeType.REMOVE)
	@JoinColumn(name = "form_id",insertable = false, updatable = false)
	private Set<EvaluationFormTypeElement> elements;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Set<EvaluationFormTypeElement> getElements() {
		return elements;
	}

	public void setElements(Set<EvaluationFormTypeElement> elemets) {
		this.elements = elemets;
	}

	@Override
	public String toString() {
		return "EvaluationFormType [id=" + id + ", name=" + name + ", elemets=" + elements + "]";
	}
	

}
