package ir.refine_punishment.from_type.model;

import java.util.Set;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import com.fasterxml.jackson.annotation.JsonTypeName;

@Entity
@DiscriminatorValue("Quantitive")
@JsonTypeName("Quantitive")

public class QuantitiveApplicationMethod extends AssessmentAndInterpretationMethod {

	@OneToMany(fetch = FetchType.EAGER, orphanRemoval = true)
	@JoinColumn(name = "application_method_id")
	@Cascade({ CascadeType.ALL })
	private Set<QuantitiveRangeInterpretation> elements;

	public Set<QuantitiveRangeInterpretation> getElements() {
		return elements;
	}

	public void setElements(Set<QuantitiveRangeInterpretation> elements) {
		this.elements = elements;
	}

	@Override
	public String interpret(String value) {
		return null;
	}

	@Override
	public String toString() {
		return "QuantitiveApplicationMethod [elements=" + elements + "]";
	}

}
