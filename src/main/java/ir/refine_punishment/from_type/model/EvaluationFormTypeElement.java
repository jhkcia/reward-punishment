package ir.refine_punishment.from_type.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import ir.refine_punishment.evaluation_criterion.model.EvaluationCriterionType;

@Entity
@Table(name = "evaluation_form_type_element")
public class EvaluationFormTypeElement {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "element_id")
	protected Long id;

	@Column(name = "criterion_id", nullable = false)
	private Long criterionId;

	@OneToMany(fetch = FetchType.EAGER ,   orphanRemoval = true)
	@JoinColumn(name = "form_element_id")
	@Cascade({ CascadeType.ALL })
	private Set<AssessmentAndInterpretationMethod> assessmentAndInterpretationMethods;

	@Column(name = "type")
	@Enumerated(EnumType.STRING)
	private EvaluationCriterionType type;

	@Column(name = "form_id", nullable = false)
	private Long formTypeId;

	public Long getCriterionId() {
		return criterionId;
	}
	
	public void setCriterionId(Long criterionId) {
		this.criterionId = criterionId;
	}
	public Long getFormTypeId() {
		return formTypeId;
	}

	public void setFormTypeId(Long formTypeId) {
		this.formTypeId = formTypeId;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}


	public EvaluationCriterionType getType() {
		return type;
	}

	public void setType(EvaluationCriterionType type) {
		this.type = type;
	}

	public Set<AssessmentAndInterpretationMethod> getAssessmentAndInterpretationMethods() {
		return assessmentAndInterpretationMethods;
	}

	public void setAssessmentAndInterpretationMethods(
			Set<AssessmentAndInterpretationMethod> assessmentAndInterpretationMethods) {
		this.assessmentAndInterpretationMethods = assessmentAndInterpretationMethods;
	}

	@Override
	public String toString() {
		return "EvaluationFormTypeElement [id=" + id + ", criterion=" + ", type=" + type
				+ ", assessmentAndInterpretationMethods=" + assessmentAndInterpretationMethods + "]";
	}

}
