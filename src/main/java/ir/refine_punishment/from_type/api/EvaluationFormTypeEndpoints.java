package ir.refine_punishment.from_type.api;

import java.util.List;

import javax.ws.rs.BadRequestException;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.core.JsonProcessingException;

import ir.refine_punishment.core.api.BaseEndPoint;
import ir.refine_punishment.core.auth.SecuredEndPoint;
import ir.refine_punishment.from_type.catalogue.EvaluationFormTypeCatalogue;
import ir.refine_punishment.from_type.model.EvaluationFormType;
import ir.refine_punishment.from_type.model.EvaluationFormTypeElement;
import ir.refine_punishment.user.model.Role;

@Path("/evaluation_form_type")
public class EvaluationFormTypeEndpoints extends BaseEndPoint {
	@Autowired
	protected EvaluationFormTypeCatalogue evaluationFormTypeCatalogue;

	@POST
	@Path("add")
	@SecuredEndPoint({ Role.ADMIN })
	public EvaluationFormType add(EvaluationFormType evaluationFormType) {
		evaluationFormTypeCatalogue.add(evaluationFormType);
		return evaluationFormTypeCatalogue.getById(evaluationFormType.getId());
	}

	@GET
	@Path("get")
	@SecuredEndPoint({ Role.ADMIN })
	public EvaluationFormType get(@QueryParam("id") Long id) {
		return evaluationFormTypeCatalogue.getById(id);
	}

	@GET
	@Path("list")
	@SecuredEndPoint({ Role.ADMIN })
	public List<EvaluationFormType> list() throws JsonProcessingException {
		List<EvaluationFormType> list = evaluationFormTypeCatalogue.getAll();
		return list;
	}

	@GET
	@Path("delete")
	@SecuredEndPoint({ Role.ADMIN })
	public void delete(@QueryParam("id") Long evaluationFormTypeId) {
		evaluationFormTypeCatalogue.delete(evaluationFormTypeId);
	}

	@POST
	@Path("update")
	@SecuredEndPoint({ Role.ADMIN })
	public EvaluationFormType update(EvaluationFormType evaluationFormType) {
		evaluationFormTypeCatalogue.update(evaluationFormType);
		return evaluationFormTypeCatalogue.getById(evaluationFormType.getId());
	}

	@POST
	@Path("addElement")
	@SecuredEndPoint({ Role.ADMIN })
	public EvaluationFormType addElement(EvaluationFormTypeElement evaluationFormTypeElement) {
		if(evaluationFormTypeElement.getFormTypeId()==null) {
			System.err.println("Type is not set...");
			throw new BadRequestException();
		}
		evaluationFormTypeCatalogue.addElementToFormType(evaluationFormTypeElement);
		return evaluationFormTypeCatalogue.getById(evaluationFormTypeElement.getFormTypeId());
	}
}
