package ir.refine_punishment.from_type.catalogue;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ir.refine_punishment.from_type.dao.EvaluationFormTypeDAO;
import ir.refine_punishment.from_type.model.EvaluationFormType;
import ir.refine_punishment.from_type.model.EvaluationFormTypeElement;

public class EvaluationFormTypeCatalogue {

	@Autowired
	EvaluationFormTypeDAO dao;

	public void add(EvaluationFormType entity) {
		dao.add(entity);
	}

	public EvaluationFormType getById(Long id) {
		return dao.getById(id);
	}

	public List<EvaluationFormType> getAll() {
		return dao.getAll();
	}

	public void update(EvaluationFormType entity) {
		dao.update(entity);
	}

	public void delete(Long id) {
		dao.delete(id);
	}
	public void addElementToFormType(EvaluationFormTypeElement element) {
		dao.addElementToFormType(element);
	}
	public void deleteElementFromFormType(String elementId) {
		dao.deleteElementFromFormType(elementId);
	}
	
}
