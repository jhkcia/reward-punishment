package ir.refine_punishment.user.dao;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;

import ir.refine_punishment.core.persistence.BaseHibernateEntityDAO;
import ir.refine_punishment.user.model.User;

public class UserDAO extends BaseHibernateEntityDAO<User> {

	public UserDAO() {
		super(User.class);
	}

	public User getUser(String username, String password) {
		Session session = this.sessionFactory.openSession();
		Query query = session.createQuery("From User where personnelCode = :pCode and password=:pwd");
		query.setParameter("pCode", username);
		query.setParameter("pwd", password);
		List<User> result = (List<User>) query.list();
		session.close();
		return getFirstOrNull(result);
	}

	public void changeToken(String username, String token) {
		Session session = this.sessionFactory.openSession();
		Query query = session.createQuery("UPDATE User u SET u.token = :tkn WHERE u.personnelCode = :pCode");
		query.setParameter("pCode", username);
		query.setParameter("tkn", token);
		query.executeUpdate();
		session.close();

	}

	public User getByToken(String token) {
		Session session = this.sessionFactory.openSession();

		Query query = session.createQuery("From User where token = :tkn");
		query.setParameter("tkn", token);
		List<User> result = (List<User>) query.list();
		session.close();
		return getFirstOrNull(result);
	}

	public List<User> getEvaluatorUsers(Long evaluatorId) {
		Session session = this.sessionFactory.openSession();

		Query query = session.createQuery("From User where evaluatorId = :evId");
		query.setParameter("evId", evaluatorId);
		List<User> result = (List<User>) query.list();
		session.close();
		return result;
	}

}
