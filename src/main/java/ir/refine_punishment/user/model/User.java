package ir.refine_punishment.user.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class User {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "user_id")
	protected Long id;

	@Column(name = "first_name", nullable = false, length = 50)
	private String firstName;

	@Column(name = "last_name", nullable = false, length = 50)
	private String lastName;

	@Column(name = "personnel_code", nullable = false, length = 50)
	private String personnelCode;

	@Column(name = "password", nullable = false, length = 50)
	private String password;
	
	@Column(name = "token", nullable = true, length = 50)
	private String token;

	@Column(name = "role_name")
	@Enumerated(EnumType.STRING)
	private Role role;

	@Column(name = "branch_id")
	private Long branchId;
	
	@Column(name = "evaluation_form_type_id", nullable=true)
	private Long evaluationFormTypeId;
	
	@Column(name = "evaluator_id", nullable=true)
	private Long evaluatorId;

	public Long getBranchId() {
		return branchId;
	}

	public void setBranchId(Long branchId) {
		this.branchId = branchId;
	}

	public Long getEvaluationFormTypeId() {
		return evaluationFormTypeId;
	}

	public void setEvaluationFormTypeId(Long evaluationFormTypeId) {
		this.evaluationFormTypeId = evaluationFormTypeId;
	}

	public Long getEvaluatorId() {
		return evaluatorId;
	}

	public void setEvaluatorId(Long evaluatorId) {
		this.evaluatorId = evaluatorId;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPersonnelCode() {
		return personnelCode;
	}

	public void setPersonnelCode(String personnelCode) {
		this.personnelCode = personnelCode;
	}

	public Role getRole() {
		return role;
	}

	public void setRole(Role role) {
		this.role = role;
	}

	public Long getId() {
		return id;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

}
