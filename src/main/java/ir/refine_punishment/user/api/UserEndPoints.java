package ir.refine_punishment.user.api;

import java.util.List;
import java.util.UUID;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;

import ir.refine_punishment.core.api.BaseEndPoint;
import ir.refine_punishment.core.auth.SecuredEndPoint;
import ir.refine_punishment.user.model.Role;
import ir.refine_punishment.user.model.User;

@Path("/user")
public class UserEndPoints extends BaseEndPoint {

	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/login")
	public User authenticateUser(@QueryParam("personnelCode") String personnelCode,
			@QueryParam("password") String password) {
		try {
			// Authenticate the user using the credentials provided
			User user = userServices.getUser(personnelCode, password);
			if (user == null)
				throw new Exception("Bad Credentials...");

			// Issue a token for the user
			String token = UUID.randomUUID().toString();
			userServices.changeToken(personnelCode, token);
			// Return the token on the response
			return userServices.getById(user.getId());

		} catch (Exception e) {
			return null;
		}
	}
	@GET
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("/test")
	public User test() {
		return userServices.getAll().get(0);
	}
	@POST
	@Path("add")
	@SecuredEndPoint({ Role.ADMIN })
	public User add(User user) {
		userServices.add(user);
		return userServices.getById(user.getId());
	}

	@GET
	@Path("get")
	@SecuredEndPoint({ Role.ADMIN })
	public User get(@QueryParam("id") Long id) {
		return userServices.getById(id);
	}

	@GET
	@Path("list")
	@SecuredEndPoint({ Role.ADMIN })
	public List<User> list() {
		List<User> list = userServices.getAll();
		return list;
	}

	@POST
	@Path("delete")
	@SecuredEndPoint({ Role.ADMIN })
	public void delete(@QueryParam("id") Long userId) {
		userServices.delete(userId);
	}

	@POST
	@Path("update")
	@SecuredEndPoint({ Role.ADMIN })
	public User update(User user) {
		userServices.update(user);
		return userServices.getById(user.getId());
	}

	@GET
	@Path("not_evaluated")
	@SecuredEndPoint({ Role.EVALUATOR })
	public List<User> notEvaluated() {
		return userServices.getNotEvaluatedUsers(getCurrectAttachedUser().getId());
	}

}
