package ir.refine_punishment.user.catalogue;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ir.refine_punishment.from_entity.catalogue.EvaluationFormEntityCatalogue;
import ir.refine_punishment.from_entity.model.EvaluationFormEntity;
import ir.refine_punishment.user.dao.UserDAO;
import ir.refine_punishment.user.model.User;

public class UserCatalogue {
	@Autowired
	UserDAO dao;
	@Autowired
	EvaluationFormEntityCatalogue formEntityCatalogue;

	public void add(User entity) {
		dao.add(entity);
	}

	public User getById(Long id) {
		return dao.getById(id);
	}

	public List<User> getAll() {
		return dao.getAll();
	}

	public void update(User entity) {
		dao.update(entity);
	}

	public void delete(Long id) {
		dao.delete(id);
	}

	public User getUser(String personnelCode, String password) {
		return dao.getUser(personnelCode, password);
	}

	public void changeToken(String personnelCode, String token) {
		dao.changeToken(personnelCode, token);
	}

	public User getByToken(String token) {
		return dao.getByToken(token);
	}

	public List<User> getEvaluatorUsers(Long evaluatorId) {
		return dao.getEvaluatorUsers(evaluatorId);
	}

	public List<User> getNotEvaluatedUsers(Long evaluatorId) {
		List<User> allUsers = getEvaluatorUsers(evaluatorId);
		List<User> notEvaluatedUsers = new ArrayList<>();
		for (User u : allUsers) {
			EvaluationFormEntity form = formEntityCatalogue.getByOwnerId(u.getId());
			if (form == null) {
				notEvaluatedUsers.add(u);
			}
		}
		return notEvaluatedUsers;
	}
}
