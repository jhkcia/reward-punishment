package ir.refine_punishment.evaluation_criterion.dao;

import ir.refine_punishment.core.persistence.BaseHibernateEntityDAO;
import ir.refine_punishment.evaluation_criterion.model.EvaluationCriterion;

public class EvaluationCriterionDAO extends BaseHibernateEntityDAO<EvaluationCriterion> {

	public EvaluationCriterionDAO() {
		super(EvaluationCriterion.class);
	}

}
