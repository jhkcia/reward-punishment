package ir.refine_punishment.evaluation_criterion.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "evaluation_criterion")
public class EvaluationCriterion {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "criterion_id")
	protected Long id;

	public Long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}
	@Column(name = "name")
	private String name;
	
	@Column(name = "type")
	@Enumerated(EnumType.STRING)
	private EvaluationCriterionType type;
	@Column(name = "reward_method")
	private String rewardMethod;
	@Column(name = "punishment_method")
	private String punishmentMethod;
	@Column(name = "no_reward_punishment_method")
	private String noRewadPunishmentMethod;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public EvaluationCriterionType getType() {
		return type;
	}

	public void setType(EvaluationCriterionType type) {
		this.type = type;
	}

	public String getRewardMethod() {
		return rewardMethod;
	}

	public void setRewardMethod(String rewardMethod) {
		this.rewardMethod = rewardMethod;
	}

	public String getPunishmentMethod() {
		return punishmentMethod;
	}

	public void setPunishmentMethod(String punishmentMethod) {
		this.punishmentMethod = punishmentMethod;
	}

	public String getNoRewadPunishmentMethod() {
		return noRewadPunishmentMethod;
	}

	public void setNoRewadPunishmentMethod(String noRewadPunishmentMethod) {
		this.noRewadPunishmentMethod = noRewadPunishmentMethod;
	}

}
