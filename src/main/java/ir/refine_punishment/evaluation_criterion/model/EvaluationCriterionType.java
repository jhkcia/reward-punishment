package ir.refine_punishment.evaluation_criterion.model;

public enum EvaluationCriterionType {
	QUANTITIVE,
	QUALITATIVE,
	QUANTITIVE_QUALITATIVE;
}
