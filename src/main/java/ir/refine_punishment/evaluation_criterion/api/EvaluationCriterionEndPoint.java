package ir.refine_punishment.evaluation_criterion.api;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

import org.springframework.beans.factory.annotation.Autowired;

import ir.refine_punishment.core.api.BaseEndPoint;
import ir.refine_punishment.core.auth.SecuredEndPoint;
import ir.refine_punishment.evaluation_criterion.catalogue.EvaluationCriterionCatalogue;
import ir.refine_punishment.evaluation_criterion.model.EvaluationCriterion;
import ir.refine_punishment.user.model.Role;
@Path("/evaluation_criterion")
public class EvaluationCriterionEndPoint extends BaseEndPoint {
	@Autowired
	protected EvaluationCriterionCatalogue evaluationCriterionCatalogue;

	@POST
	@Path("add")
	@SecuredEndPoint({ Role.ADMIN })
	public EvaluationCriterion add(EvaluationCriterion evaluationCriterion) {
		evaluationCriterionCatalogue.add(evaluationCriterion);
		return evaluationCriterionCatalogue.getById(evaluationCriterion.getId());
	}

	@GET
	@Path("get")
	@SecuredEndPoint({ Role.ADMIN })
	public EvaluationCriterion get(@QueryParam("id") Long id) {
		return evaluationCriterionCatalogue.getById(id);
	}

	@GET
	@Path("list")
	@SecuredEndPoint({ Role.ADMIN })
	public List<EvaluationCriterion> list() {
		List<EvaluationCriterion> list = evaluationCriterionCatalogue.getAll();
		return list;
	}

	@DELETE
	@Path("delete")
	@SecuredEndPoint({ Role.ADMIN })
	public void delete(@QueryParam("id") Long evaluationCriterionId) {
		evaluationCriterionCatalogue.delete(evaluationCriterionId);
	}
	
	@POST
	@Path("update")
	@SecuredEndPoint({ Role.ADMIN })
	public EvaluationCriterion update(EvaluationCriterion evaluationCriterion) {
		evaluationCriterionCatalogue.update(evaluationCriterion);
		return evaluationCriterionCatalogue.getById(evaluationCriterion.getId());
	}

}