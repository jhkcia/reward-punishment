package ir.refine_punishment.evaluation_criterion.catalogue;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ir.refine_punishment.evaluation_criterion.dao.EvaluationCriterionDAO;
import ir.refine_punishment.evaluation_criterion.model.EvaluationCriterion;

public class EvaluationCriterionCatalogue {

	@Autowired
	EvaluationCriterionDAO dao;

	public void add(EvaluationCriterion entity) {
		dao.add(entity);
	}

	public EvaluationCriterion getById(Long id) {
		return dao.getById(id);
	}

	public List<EvaluationCriterion> getAll() {
		return dao.getAll();
	}

	public void update(EvaluationCriterion entity) {
		dao.update(entity);
	}

	public void delete(Long id) {
		dao.delete(id);
	}

}
