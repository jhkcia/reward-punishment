package ir.refine_punishment.core.persistence;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class BaseHibernateEntityDAO<T> {
	@Autowired
	protected SessionFactory sessionFactory;
	private final Class<T> clazz;

	public BaseHibernateEntityDAO(Class<T> clazz) {
		this.clazz = clazz;
	}

	public void add(T entity) {
		Session session = this.sessionFactory.openSession();
		Transaction tx = session.beginTransaction();
		session.persist(entity);
		tx.commit();
		session.close();
	}

	public T getById(Long id) {
		Session session = this.sessionFactory.openSession();
		Object item = session.get(clazz, id);
		session.close();
		return (T) item;
	}

	public List<T> getAll() {
		Session session = this.sessionFactory.openSession();
		List<T> personList = session.createQuery("from " + clazz.getName()).list();
		session.close();
		return personList;
	}

	public void update(T entity) {
		Session session = sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		session.update(entity);
		transaction.commit();
		session.close();
	}

	public void delete(Long id) {
		Session session = this.sessionFactory.openSession();
		Transaction transaction = session.beginTransaction();
		Object item = session.get(clazz, id);
		session.delete(item);
		transaction.commit();
		session.close();
	}

	protected T getFirstOrNull(List<T> input) {
		if (input == null || input.size() == 0)
			return null;
		return input.get(0);
	}
}
