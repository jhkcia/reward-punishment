package ir.refine_punishment.core.api;

import javax.ws.rs.Consumes;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.SecurityContext;

import org.springframework.beans.factory.annotation.Autowired;

import ir.refine_punishment.user.catalogue.UserCatalogue;
import ir.refine_punishment.user.model.User;

@Consumes(MediaType.APPLICATION_JSON)
@Produces(MediaType.APPLICATION_JSON)
public class BaseEndPoint {
	@Autowired
	protected UserCatalogue userServices;
	@Context
	protected SecurityContext securityContext;

	protected User getCurrectAttachedUser() {
		return userServices.getById(Long.valueOf(securityContext.getUserPrincipal().getName()));
	}
}
