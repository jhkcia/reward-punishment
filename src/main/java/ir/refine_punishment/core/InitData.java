package ir.refine_punishment.core;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.stereotype.Component;

import ir.refine_punishment.branch.dao.BranchDAO;
import ir.refine_punishment.branch.model.Branch;
import ir.refine_punishment.core.context.StaticContextAccessor;
import ir.refine_punishment.evaluation_criterion.catalogue.EvaluationCriterionCatalogue;
import ir.refine_punishment.evaluation_criterion.model.EvaluationCriterion;
import ir.refine_punishment.evaluation_criterion.model.EvaluationCriterionType;
import ir.refine_punishment.from_type.catalogue.EvaluationFormTypeCatalogue;
import ir.refine_punishment.from_type.model.AssessmentAndInterpretationMethod;
import ir.refine_punishment.from_type.model.EvaluationFormType;
import ir.refine_punishment.from_type.model.EvaluationFormTypeElement;
import ir.refine_punishment.from_type.model.EvaluationInterpretion;
import ir.refine_punishment.from_type.model.QuantitiveApplicationMethod;
import ir.refine_punishment.from_type.model.QuantitiveRangeInterpretation;
import ir.refine_punishment.user.dao.UserDAO;
import ir.refine_punishment.user.model.Role;
import ir.refine_punishment.user.model.User;

@Component
public class InitData {

	public static void main(String[] args) {
		StaticContextAccessor.initializeContext();
		StaticContextAccessor.printBeans();

		EvaluationFormTypeElement elemet = new EvaluationFormTypeElement();
		elemet.setCriterionId(1L);
		elemet.setFormTypeId(1L);
		elemet.setType(EvaluationCriterionType.QUANTITIVE_QUALITATIVE);
		Set<AssessmentAndInterpretationMethod> assessmentAndInterpretationMethods = new HashSet<>();

		QuantitiveApplicationMethod q = new QuantitiveApplicationMethod();
		Set<QuantitiveRangeInterpretation> ranges = new HashSet<>();
		QuantitiveRangeInterpretation r = new QuantitiveRangeInterpretation();
		r.setLowerBound(1);
		r.setUpperBound(100);
		r.setInterpretation(EvaluationInterpretion.Punishment);
		ranges.add(r);
		// ranges.add(new QuantitiveRangeInterpretation(2, 100,
		// EvaluationInterpretion.NoRewardAndPunishment, q));
		// ranges.add(new QuantitiveRangeInterpretation(3, 100,
		// EvaluationInterpretion.Reward, q));
		q.setElements(ranges);
//		 q.setFormElementId(elemet.getId());
		assessmentAndInterpretationMethods.add(q);
		EvaluationFormTypeCatalogue evFromTypeCatalogue = StaticContextAccessor
				.getBean(EvaluationFormTypeCatalogue.class);

		elemet.setAssessmentAndInterpretationMethods(assessmentAndInterpretationMethods);
		evFromTypeCatalogue.addElementToFormType(elemet);

		//init();

		List<EvaluationFormType> lst = evFromTypeCatalogue.getAll();
		System.out.println(lst);

		// EvaluationFormType formType = lst.get(0);
		// formType.setName("Jalal");
		// evFromTypeCatalogue.add(formType);
		// for(EvaluationFormType t:lst) {
		// t.setElemets(null);
		// evFromTypeCatalogue.update(t);
		// if(t.getElemets()!=null&&t.getElemets().size()>0)
		// evFromTypeCatalogue.delete(t.getId());
		// }
		//
		// EvaluationCriterionCatalogue evCatalogue =
		// StaticContextAccessor.getBean(EvaluationCriterionCatalogue.class);
		//
		// EvaluationFormTypeElement elemet = new EvaluationFormTypeElement();
		// elemet.setCriterion(evCatalogue.getAll().get(0));
		// elemet.setFormType(formType);
		// elemet.setType(EvaluationCriterionType.QUANTITIVE_QUALITATIVE);
		// Set<AssessmentAndInterpretationMethod> assessmentAndInterpretationMethods =
		// new HashSet<>();
		//
		// QuantitiveApplicationMethod q = new QuantitiveApplicationMethod();
		// Set<QuantitiveRangeInterpretation> ranges = new HashSet<>();
		// ranges.add(new QuantitiveRangeInterpretation(1, 100,
		// EvaluationInterpretion.Punishment, q));
		// ranges.add(new QuantitiveRangeInterpretation(2, 100,
		// EvaluationInterpretion.NoRewardAndPunishment, q));
		// ranges.add(new QuantitiveRangeInterpretation(3, 100,
		// EvaluationInterpretion.Reward, q));
		// q.setElemets(ranges);
		// q.setFormElement(elemet);
		// assessmentAndInterpretationMethods.add(q);
		//
		// elemet.setAssessmentAndInterpretationMethods(assessmentAndInterpretationMethods);
		// evFromTypeCatalogue.addElementToFormType(formType, elemet);

	}

	private static void init() {
		Branch b = new Branch();
		b.setAddress("Tehran");
		b.setName("Central");
		b.setPhoneNumber("021-2222222222");
		BranchDAO dd = StaticContextAccessor.getBean(BranchDAO.class);
		dd.add(b);

		User user1 = new User();
		user1.setFirstName("Jalal");
		user1.setLastName("Heidari");
		user1.setPersonnelCode("92106418");
		user1.setPassword("92106418");
		user1.setRole(Role.ADMIN);
		user1.setBranchId(b.getId());
		UserDAO dao = StaticContextAccessor.getBean(UserDAO.class);
		dao.add(user1);

		User user2 = new User();
		user2.setFirstName("Alireza");
		user2.setLastName("Khajooei");
		user2.setPersonnelCode("92108262");
		user2.setPassword("92108262");
		user2.setRole(Role.EVALUATOR);
		user2.setBranchId(b.getId());
		user2.setEvaluatorId(user1.getId());
		dao.add(user2);

		User user3 = new User();
		user3.setFirstName("Ali");
		user3.setLastName("Alinejad");
		user3.setPersonnelCode("92100000");
		user3.setPassword("92100000");
		user3.setRole(Role.USER);
		user3.setBranchId(b.getId());
		user3.setEvaluatorId(user2.getId());
		dao.add(user3);

		EvaluationCriterionCatalogue evCatalogue = StaticContextAccessor.getBean(EvaluationCriterionCatalogue.class);
		EvaluationCriterion critTajrobe = new EvaluationCriterion();
		critTajrobe.setName("تجربه");
		critTajrobe.setType(EvaluationCriterionType.QUANTITIVE_QUALITATIVE);
		critTajrobe.setNoRewadPunishmentMethod("عدم تغییر حقوق");
		critTajrobe.setPunishmentMethod("کاهش ده درصدی حقوق");
		critTajrobe.setRewardMethod("افزایش ده درصدی حقوق");
		evCatalogue.add(critTajrobe);

		EvaluationCriterion critNazm = new EvaluationCriterion();
		critNazm.setName("نظم");
		critNazm.setType(EvaluationCriterionType.QUALITATIVE);
		critNazm.setNoRewadPunishmentMethod("عدم تغییر حقوق");
		critNazm.setPunishmentMethod("100 هزار پاداش");
		critNazm.setRewardMethod("۵۰ هزار جریمه");
		evCatalogue.add(critNazm);

		EvaluationCriterion critWorkHours = new EvaluationCriterion();
		critWorkHours.setName("ساعت کاری");
		critWorkHours.setType(EvaluationCriterionType.QUANTITIVE);
		critWorkHours.setNoRewadPunishmentMethod("عدم تغییر حقوق");
		critWorkHours.setPunishmentMethod("کاهش ده درصدی حقوق");
		critWorkHours.setRewardMethod("افزایش ده درصدی حقوق");
		evCatalogue.add(critWorkHours);

		EvaluationFormTypeCatalogue evFromTypeCatalogue = StaticContextAccessor
				.getBean(EvaluationFormTypeCatalogue.class);

		EvaluationFormType formType = new EvaluationFormType();
		formType.setName("فرم ارزیابی سال  تازه واردها");
		evFromTypeCatalogue.add(formType);

		EvaluationFormTypeElement elemet = new EvaluationFormTypeElement();
		elemet.setCriterionId(critTajrobe.getId());
		elemet.setFormTypeId(formType.getId());
		elemet.setType(EvaluationCriterionType.QUANTITIVE_QUALITATIVE);
		Set<AssessmentAndInterpretationMethod> assessmentAndInterpretationMethods = new HashSet<>();

		QuantitiveApplicationMethod q = new QuantitiveApplicationMethod();
		Set<QuantitiveRangeInterpretation> ranges = new HashSet<>();
		QuantitiveRangeInterpretation r = new QuantitiveRangeInterpretation();
		r.setLowerBound(1);
		r.setUpperBound(100);
		r.setInterpretation(EvaluationInterpretion.Punishment);
		ranges.add(r);
		// ranges.add(new QuantitiveRangeInterpretation(2, 100,
		// EvaluationInterpretion.NoRewardAndPunishment, q));
		// ranges.add(new QuantitiveRangeInterpretation(3, 100,
		// EvaluationInterpretion.Reward, q));
		q.setElements(ranges);
		// q.setFormElementId(elemet.getId());
		assessmentAndInterpretationMethods.add(q);

		elemet.setAssessmentAndInterpretationMethods(assessmentAndInterpretationMethods);
		evFromTypeCatalogue.addElementToFormType(elemet);
	}
}
