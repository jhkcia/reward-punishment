package ir.refine_punishment.branch.dao;

import ir.refine_punishment.branch.model.Branch;
import ir.refine_punishment.core.persistence.BaseHibernateEntityDAO;

public class BranchDAO extends BaseHibernateEntityDAO<Branch> {
	public BranchDAO() {
		super(Branch.class);
	}
}
