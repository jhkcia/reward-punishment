package ir.refine_punishment.branch.api;

import java.util.List;

import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;

import org.springframework.beans.factory.annotation.Autowired;

import ir.refine_punishment.branch.catalogue.BranchCatalogue;
import ir.refine_punishment.branch.model.Branch;
import ir.refine_punishment.core.api.BaseEndPoint;
import ir.refine_punishment.core.auth.SecuredEndPoint;
import ir.refine_punishment.user.model.Role;

@Path("/branch")
public class BranchEndPoint extends BaseEndPoint {
	@Autowired
	protected BranchCatalogue branchCatalogue;

	@POST
	@Path("add")
	@SecuredEndPoint({ Role.ADMIN })
	public Branch add(Branch branch) {
		branchCatalogue.add(branch);
		return branchCatalogue.getById(branch.getId());
	}

	@GET
	@Path("get")
	@SecuredEndPoint({ Role.ADMIN })
	public Branch get(@QueryParam("id") Long id) {
		return branchCatalogue.getById(id);
	}

	@GET
	@Path("list")
	@SecuredEndPoint({ Role.ADMIN })
	public List<Branch> list() {
		List<Branch> list = branchCatalogue.getAll();
		return list;
	}

	@DELETE
	@Path("delete")
	@SecuredEndPoint({ Role.ADMIN })
	public void delete(@QueryParam("id") Long branchId) {
		branchCatalogue.delete(branchId);
	}

	@POST
	@Path("update")
	@SecuredEndPoint({ Role.ADMIN })
	public Branch update(Branch branch) {
		branchCatalogue.update(branch);
		return branchCatalogue.getById(branch.getId());
	}
}
