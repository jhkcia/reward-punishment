package ir.refine_punishment.branch.catalogue;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import ir.refine_punishment.branch.dao.BranchDAO;
import ir.refine_punishment.branch.model.Branch;

public class BranchCatalogue {
	@Autowired
	BranchDAO dao;

	public void add(Branch entity) {
		dao.add(entity);
	}

	public Branch getById(Long id) {
		return dao.getById(id);
	}

	public List<Branch> getAll() {
		return dao.getAll();
	}

	public void update(Branch entity) {
		dao.update(entity);
	}

	public void delete(Long id) {
		dao.delete(id);
	}
}
